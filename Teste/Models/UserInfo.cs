﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Teste.Models
{
    public class UserInfo
    {
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Password { get; set; }
        public bool Ativo { get; set; }
        public bool Admin { get; set; }
    }
}
