﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Teste.Models
{
    public class FilmesVoto
    {
        [Key]
        [JsonIgnore]
        public int id { get; set; }
        [JsonIgnore]
        public string Usuario { get; set; }
        [DataMember]
        public string Filme { get; set; }
        [DataMember]
        public int Voto { get; set; }

    }
}
