﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Teste.Models
{
    public class ApplicationUser : IdentityUser
    {
        public bool ativo { get; set; }
        public bool admin { get; set; }
    }
}
