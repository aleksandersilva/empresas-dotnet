﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Teste.Models
{
    public class Filmes
    {
        [Key]
        [JsonIgnore]
        public int id { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public DateTime DataLancamento { get; set; }
        [DataMember]
        public string diretor { get; set; }
        [DataMember]
        public string genero { get; set; }
        [DataMember]
        public string Atores { get; set; }
        [DataMember]
        public int nota { get; set; }

    }
}
