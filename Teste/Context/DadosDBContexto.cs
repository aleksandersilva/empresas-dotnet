﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Teste.Models;

namespace Teste.Context
{
    public class DadosDBContexto : DbContext
    {
        public DadosDBContexto(DbContextOptions<DadosDBContexto> options) : base(options)
        { }
        public DbSet<Filmes> Filmes { get; set; }
        public DbSet<FilmesVoto> FilmesVoto { get; set; }

    }
}
