﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Teste.Models;

namespace Teste.Context
{
    public class AplicacaoDBContexto : IdentityDbContext<ApplicationUser>
    {
        public AplicacaoDBContexto(DbContextOptions<AplicacaoDBContexto> options) : base(options)
        {
        }
    }
}
