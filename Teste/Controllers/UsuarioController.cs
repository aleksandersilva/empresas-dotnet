﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Teste.Models;

namespace WebApiUsuarios.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;

        public UsuariosController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IConfiguration configuration)
        {
            _userManager = userManager;
            _userManager.Options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+/ ";
            // Remove the restriction on only alphanumeric names 

            _signInManager = signInManager;
            _configuration = configuration;
        }

        [HttpGet]
        public ActionResult<string> Get()
        {
            return "Controlador UsuariosController :: Teste";
        }

        [HttpPost("Criar")]
        [AllowAnonymous]
        public async Task<ActionResult<UserToken>> CreateUser([FromBody] UserInfo model)
        {
            try
            {
                var user = new ApplicationUser { UserName = model.Nome, Email = model.Email, ativo = model.Ativo, admin = model.Admin };

                IdentityResult result;
                var buscar = await _userManager.FindByNameAsync(model.Nome);
                if (buscar != null && !string.IsNullOrEmpty(buscar.Id))
                {
                    buscar.Email = user.Email;
                    await _userManager.RemovePasswordAsync(buscar);
                    await _userManager.AddPasswordAsync(buscar, model.Password);
                    buscar.ativo = user.ativo;
                    buscar.admin = user.admin;
                    result = await _userManager.UpdateAsync(buscar);
                }
                else
                    result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    return Ok("Usuario Inserido/Atualizado");
                    //return BuildToken(model);
                }
                else
                {
                    var errList = "";
                    var error = result.Errors.ToList();

                    foreach (var err in error)
                    {
                        errList += "\n" + string.Join(", ", err.Description);
                    }

                    return BadRequest("Usuário ou senha inválidos Motivos: " + errList);
                }
            }
            catch (Exception)
            {

                return BadRequest("erro"); ;
            }
        }

        [HttpGet ("ListarUsuario")]
        //[Authorize("admin")]
        public async Task<ActionResult<UserToken>> ListarUser(string nome,string senha)
        {
            try
            {
                var result = await _signInManager.PasswordSignInAsync(nome, senha, isPersistent: false, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    var user = await _userManager.FindByNameAsync(nome);
                    if (user!= null && !string.IsNullOrEmpty(user.Id) && user.admin)
                    {
                        var userresultados = _userManager.Users.ToList().Where(x=>x.admin==false && x.ativo).Select(c => c.UserName);
                        
                        return Ok(JsonSerializer.Serialize(userresultados));
                    }
                    else
                    {
                        return BadRequest("Não Foi possivel listar");
                    }
                }
                else
                {
                    return BadRequest("Login Invalido");
                }
            }
            catch (Exception)
            {

                return BadRequest("erro"); ;
            }
        }

        [HttpPost("Login")]
        [AllowAnonymous]
        public async Task<ActionResult<UserToken>> Login(string nome, string senha)
        {
            var result = await _signInManager.PasswordSignInAsync(nome, senha, isPersistent: false, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                UserInfo user = new UserInfo();
                user.Nome = nome;
                user.Password = senha;
                user.Admin = _signInManager.UserManager.Users.FirstOrDefault().admin;
                return BuildToken(user);
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                return BadRequest(ModelState);
            }
        }

        private UserToken BuildToken(UserInfo userInfo)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, userInfo.Nome),
                new Claim("Teste", userInfo.Admin== true ? "admin":"user"),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            // Tiempo de expiración del token. En nuestro caso lo hacemos de una hora.
            var expiration = DateTime.UtcNow.AddHours(1);

            JwtSecurityToken token = new JwtSecurityToken(
               issuer: null,
               audience: null,
               claims: claims,
               expires: expiration,
               signingCredentials: creds);

            return new UserToken()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = expiration
            };
        }
    }
}