﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Teste.Context;
using Teste.Models;

namespace WebApiUsuarios.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FilmeController : ControllerBase
    {
        private readonly DadosDBContexto _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;

        public FilmeController(DadosDBContexto context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            IConfiguration configuration)
        {
            _context = context;
            _signInManager = signInManager;
            _configuration = configuration;
            _userManager = userManager;
        }

        [HttpGet]
        public ActionResult<string> Get()
        {
            return "Controlador FilmesController :: Teste";
        }

        [HttpPost("CriarFilme")]
        [AllowAnonymous]
        public async Task<ActionResult<Filmes>> CreateFilme(string nomeUsuario, string senhaUsuario,[FromBody] Filmes model)
        {
            try
            {
                var resultuser = await _signInManager.PasswordSignInAsync(nomeUsuario, senhaUsuario, isPersistent: false, lockoutOnFailure: false);
                if (resultuser.Succeeded)
                {
                    var user = await _userManager.FindByNameAsync(nomeUsuario);
                    if (user != null && !string.IsNullOrEmpty(user.Id) && user.admin)
                    {
                        var filme = new Filmes { Nome = model.Nome, DataLancamento = model.DataLancamento, diretor = model.diretor, genero = model.genero, Atores = model.Atores };
                        int result = 0;
                        var buscar = _context.Filmes.Where(x=>x.Nome == model.Nome).FirstOrDefault();
                        if (buscar != null && buscar.id > 0)
                        {
                            buscar.Nome = filme.Nome;
                            buscar.DataLancamento = filme.DataLancamento;
                            buscar.diretor = filme.diretor;
                            _context.Filmes.Update(buscar);
                            result = _context.SaveChanges();
                        }
                        else
                        {
                            _context.Filmes.Add(filme);
                            result = _context.SaveChanges();
                        }

                        if (result > 0)
                        {
                            return Ok("Filme Inserido/Atualizado");
                        }
                        else
                        {
                            return BadRequest("Não Foi possivel inserir o Filme");
                        }
                    }
                    else
                    {
                        return BadRequest("Usuario Não pode inserir/Atualizar");
                    }
                }
                else
                {
                    return BadRequest("Login Invalido");
                }
            }
            catch (Exception ex)
            {

                return BadRequest("erro: "+ ex.Message);
            }
        }
        [HttpPost("VotarFilme")]
        public async Task<ActionResult<Filmes>> CreateVotoFilme(string nomeUsuario, string senhaUsuario, [FromBody] FilmesVoto model)
        {
            try
            {
                var resultuser = await _signInManager.PasswordSignInAsync(nomeUsuario, senhaUsuario, isPersistent: false, lockoutOnFailure: false);
                if (resultuser.Succeeded)
                {
                    var user = await _userManager.FindByNameAsync(nomeUsuario);
                    if (user != null && !string.IsNullOrEmpty(user.Id) && !user.admin)
                    {
                        if (model.Voto > 4)
                            return BadRequest("Não Foi possivel inserir o Voto do Filme");
                        var filmevoto = new FilmesVoto { Filme = model.Filme, Usuario = nomeUsuario, Voto = model.Voto };
                        int result = 0;
                        var buscar = _context.Filmes.Where(x => x.Nome == model.Filme).FirstOrDefault();
                        if (buscar != null && buscar.id > 0)
                        {
                            var buscarvoto = _context.FilmesVoto.Where(x => x.Filme == model.Filme && x.Usuario == nomeUsuario).FirstOrDefault();
                            if (buscarvoto != null && buscarvoto.id > 0)
                            {
                                buscarvoto.Voto = model.Voto;
                                _context.FilmesVoto.Update(buscarvoto);
                            }
                            else
                            {
                                _context.FilmesVoto.Add(filmevoto);
                            }
                            result = _context.SaveChanges();
                        }

                        if (result > 0)
                        {
                            return Ok("Voto do Filme Inserido/Atualizado");
                        }
                        else
                        {
                            return BadRequest("Não Foi possivel inserir o Voto do Filme");
                        }
                    }
                    else
                    {
                        return BadRequest("Usuario Não pode inserir/Atualizar");
                    }
                }
                else
                {
                    return BadRequest("Login Invalido");
                }
            }
            catch (Exception ex)
            {

                return BadRequest("erro: " + ex.Message);
            }
        }
        [HttpGet("ListarFilme")]
        public async Task<ActionResult<Filmes>> ListarVotoFilme(string nomeUsuario, string senhaUsuario, string filme,string diretor,string genero)
        {
            try
            {
                var resultuser = await _signInManager.PasswordSignInAsync(nomeUsuario, senhaUsuario, isPersistent: false, lockoutOnFailure: false);
                if (resultuser.Succeeded)
                {
                    var user = await _userManager.FindByNameAsync(nomeUsuario);
                    if (user != null && !string.IsNullOrEmpty(user.Id))
                    {
                        var buscarFilmes = _context.Filmes.ToList();
                        if (!string.IsNullOrEmpty(filme))
                            buscarFilmes = buscarFilmes.Where(x => x.Nome == filme).ToList();
                        if (!string.IsNullOrEmpty(diretor))
                            buscarFilmes = buscarFilmes.Where(x => x.diretor == diretor).ToList();
                        if (!string.IsNullOrEmpty(genero))
                            buscarFilmes = buscarFilmes.Where(x => x.genero == genero).ToList();

                        if (buscarFilmes != null)
                        {
                            foreach (var item in buscarFilmes)
                            {
                                var buscarvoto = _context.FilmesVoto.Where(x => x.Filme == item.Nome).Sum(s=>s.Voto)/_userManager.Users.ToList().Where(x =>x.ativo).Count();
                                item.nota = buscarvoto;
                            }
                            return Ok(JsonSerializer.Serialize(buscarFilmes));
                        }
                        else
                        {
                            return BadRequest("Não Foi possivel Lista os Filmes");
                        }
                    }
                    else
                    {
                        return BadRequest("Não Foi possivel Lista os Filmes");
                    }
                }
                else
                {
                    return BadRequest("Login Invalido");
                }
            }
            catch (Exception ex)
            {

                return BadRequest("erro: " + ex.Message);
            }
        }
    }
}